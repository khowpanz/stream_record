from __future__ import unicode_literals

from django.db import models
from camera.models import Camera
from car.models import Car
from account.models import Account
# Create your models here.

class Operation(models.Model):
    name = models.CharField(max_length=200, null=False)
    camera = models.ManyToManyField(Camera)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    car = models.ForeignKey(Car, on_delete=models.CASCADE)
    creator = models.ForeignKey(Account, on_delete=models.CASCADE,  related_name='creator')
    head = models.ForeignKey(Account, on_delete=models.CASCADE,  related_name='head')
    active = models.BooleanField(default=True)

    def __str__(self):
        return self.name

    # class Meta:
    #      ordering = ['created_at']
