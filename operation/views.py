from rest_framework import viewsets, mixins
from rest_framework import status
from rest_framework.response import Response
from django.db.models import Sum, Count
from .models import Operation
from car.models import Car
from account.models import Account
from camera.models import Camera, Type
from account.permissions import IsSuperAdmin, IsUserAdmin, IsUser
from rest_framework.decorators import list_route
from rest_framework.permissions import IsAuthenticated
from django.utils import timezone
from .serializers import OperationSerializer, AddOperationSerializer, DeleteOperationSerializer, EditOperationSerializer, \
                        ChooseOperationSerializer, FinishOperationSerializer, SearchOperationSerializer, CheckCarOperationSerializer
from camera.serializers import CameraSerializer
from datetime import timedelta, datetime
# Create your views here.

class OperationViewSet(viewsets.GenericViewSet):
    queryset = Operation.objects.all()
    action_serializers = {
        'choosen_operation': ChooseOperationSerializer,
        'add_operation': AddOperationSerializer,
        'delete_operation': DeleteOperationSerializer,
        'edit_operation': EditOperationSerializer,
        'finish_operation': FinishOperationSerializer,
        'search_operation': SearchOperationSerializer,
        'specific_operation': ChooseOperationSerializer
    }

    def get_serializer_class(self):
        if hasattr(self, 'action_serializers'):
            if self.action in self.action_serializers:
                return self.action_serializers[self.action]
            return super().get_serializer_class()

    def get_permissions(self):
        self.permission_classes = [IsUser, ]
        return super(OperationViewSet, self).get_permissions()

    def list(self, request):
        queryset = self.get_queryset()
        serializer = OperationSerializer(queryset, many=True)
        return Response(serializer.data)

    @list_route(methods=['get'], url_path='get_all_operation')
    def get_all_operation(self, request):
        queryset = self.get_queryset()
        serializer = OperationSerializer(queryset, many=True)
        return Response(serializer.data)

    @list_route(methods=['post'], url_path='specific')
    def specific_operation(self, request):
        serializer = self.get_serializer(data=request.data)
        queryset = self.get_queryset()
        if serializer.is_valid():
            queryset = queryset.filter(id=serializer.data['id'])
            serializer = OperationSerializer(queryset, many=True)
            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @list_route(methods=['get'], url_path='specific_live')
    def specific_live_operation(self, request):
        queryset = self.get_queryset()
        queryset = queryset.filter(head=request.user.id, active=True)
        serializer = OperationSerializer(queryset, many=True)
        return Response(serializer.data)

    @list_route(methods=['post'], url_path='choosen')
    def choosen_operation(self, request):
        serializer = self.get_serializer(data=request.data)
        queryset = self.get_queryset()
        if serializer.is_valid():
            queryset = queryset.filter(id=serializer.data['id'])
            type_camera = []
            # camera = queryset.values('camera__type').annotate(count=Count('camera__type'))
            queryset = queryset.first().camera.all()
            type_all = queryset.values('type').annotate(count=Count('type'))
            response = []
            if type_all:
                for type_name in type_all:
                    type = Type.objects.filter(id=type_name['type']).first()
                    type_camera.append(type.name)
                    response.append({ type.name : []})
            for res, type_cam in zip(response, type_camera):
                camera = queryset.filter(type__name=str(type_cam))
                serializer = CameraSerializer(camera, many=True)
                res.update({ type_cam : serializer.data})
            return Response(response, status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


    @list_route(methods=['post'], url_path='add')
    def add_operation(self, request):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            car = Car.objects.filter(id=serializer.data['car']).first()
            head = Account.objects.filter(id=serializer.data['head']).first()
            creator = Account.objects.filter(id=request.user.id).first()
            operation_old = Operation.objects.filter(head=serializer.data['head'])
            operation_old = operation_old.filter(active=True).last()
            if operation_old:
                operation_old.active = False
                operation_old.save()
            operation = Operation.objects.create(
                name = serializer.data['name'],
                car = car,
                creator = creator,
                head = head
            )
            operation.save()
            for camera in serializer.data['camera']:
                operation.camera.add(Camera.objects.filter(id=camera).first())
            return Response(status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @list_route(methods=['post'], url_path='delete')
    def delete_operation(self, request):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            id = serializer.data['id']
            operation_target = Operation.objects.filter(id=id).first()
            if not operation_target:
                return Response({'error': 'operation for delete not found'}, status=status.HTTP_404_NOT_FOUND)
            operation_target.delete()
            return Response(status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @list_route(methods=['post'], url_path='edit')
    def edit_operation(self, request):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            id = serializer.data['id']
            operation_target = Operation.objects.filter(id=id).first()
            if not operation_target:
                return Response({'error': 'operation for edit not found'}, status=status.HTTP_404_NOT_FOUND)
            if serializer.data['name']:
                operation_target.name = serializer.data['ip']
            if serializer.data['car']:
                operation_target.car = Car.objects.filter(id=serializer.data['car']).first()
            if serializer.data['head']:
                operation_target.head = Account.objects.filter(id=serializer.data['head']).first()
            operation_target.creator = Account.objects.filter(id=request.user.id).first()
            operation_target.save()
            if serializer.data['camera']:
                operation_target.camera.clear()
                for camera in serializer.data['camera']:
                    operation_target.camera.add(Camera.objects.filter(id=camera).first())
            return Response(status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @list_route(methods=['post'], url_path='finish')
    def finish_operation(self, request):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            queryset = self.get_queryset()
            if serializer.data['id']==None:
                queryset = queryset.filter(head=request.user.id).first()
                queryset.active = False
                queryset.save()
            else:
                queryset = queryset.filter(head=serializer.data['id']).first()
                queryset.active = False
                queryset.save()
            return Response(status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @list_route(methods=['post'], url_path='search')
    def search_operation(self, request):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            queryset = self.get_queryset()
            end_date = datetime.strptime(serializer.data['end_date'],'%Y-%m-%d') + timedelta(days=1)
            if start_date and end_date:
                # date_search[1] = date_search + timedelta(days=1)
                queryset = queryset.filter(created_at__range=[start_date,end_date])
            if request.user.role >= 2:
                queryset = queryset.filter(head__department=request.user.department)
            serializer = OperationSerializer(queryset, many=True)
            return Response(serializer.data,status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class CheckCarActivevViewSet(mixins.CreateModelMixin, viewsets.GenericViewSet):
    queryset = Operation.objects.all()
    serializer_class = CheckCarOperationSerializer

    def create(self, request):
        serializer = self.get_serializer(data=request.data)
        queryset = self.get_queryset()
        if serializer.is_valid():
            operation_active = queryset.filter(active=True)
            operation_target = operation_active.filter(car__name=serializer.data['name'])
            operation_target = operation_active.filter(car__ip=serializer.data['ip']).first()
            if operation_target:
                response = { "check" : True }
            else:
                response = { "check" : False }
            return Response(response, status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
