# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from .models import Operation
from django.contrib import admin

# Register your models here.
class OperationAdmin(admin.ModelAdmin):
	list_display = ('id', 'name', 'created_at', 'updated_at', 'get_car', 'get_creator', 'get_head', 'active', 'get_camera')

	def get_car(self, operation):
		return operation.car.name if operation.car else "-"
	get_car.short_description = 'car'

	def get_creator(self, operation):
		return operation.creator.email if operation.creator else "-"
	get_creator.short_description = 'creator'

	def get_head(self, operation):
		return operation.head.email if operation.head else "-"
	get_head.short_description = 'head'

	def get_camera(self, obj):
		return "\n".join([p.name for p in obj.camera.all()])
	get_head.short_description = 'camera'

admin.site.register(Operation, OperationAdmin)
