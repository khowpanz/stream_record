from rest_framework import serializers
from .models import Operation
from camera.serializers import CameraSerializer

class OperationSerializer(serializers.ModelSerializer):
    camera = CameraSerializer(read_only=True, many=True)
    car = serializers.SerializerMethodField()
    creator = serializers.SerializerMethodField()
    head = serializers.SerializerMethodField()
    department = serializers.SerializerMethodField()
    class Meta:
        model = Operation
        fields = ('id', 'name', 'camera', 'created_at', 'updated_at', 'car', 'creator', 'head', 'active', 'department')

    def get_car(self, OP):
        return OP.car.name if OP.car else "-"

    def get_creator(self, OP):
        return OP.creator.email if OP.creator else "-"

    def get_head(self, OP):
        return OP.head.email if OP.head else "-"

    def get_department(self, OP):
        return OP.head.department.name if OP.head else "-"


class AddOperationSerializer(serializers.Serializer):
    name = serializers.CharField(max_length=100)
    camera = serializers.ListField(child=serializers.IntegerField())
    car = serializers.IntegerField(required=True)
    head = serializers.IntegerField(required=True)


class DeleteOperationSerializer(serializers.Serializer):
    id = serializers.IntegerField(required=True)


class EditOperationSerializer(serializers.Serializer):
    id = serializers.IntegerField(required=True)
    name = serializers.CharField(max_length=100, allow_null=True)
    camera = serializers.ListField(child=serializers.IntegerField())
    car = serializers.IntegerField(allow_null=True)
    head = serializers.IntegerField(allow_null=True)


class ChooseOperationSerializer(serializers.Serializer):
    id = serializers.IntegerField(required=True)


class FinishOperationSerializer(serializers.Serializer):
    id = serializers.IntegerField(allow_null=True)


class SearchOperationSerializer(serializers.Serializer):
    start_date = serializers.DateField()
    end_date = serializers.DateField()


class CheckCarOperationSerializer(serializers.Serializer):
    name = serializers.CharField(max_length=100)
    ip = serializers.CharField(max_length=100)
