"""stream_record URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
# from django.urls import path
from django.conf import settings
from django.conf.urls import url
from django.conf.urls.static import static
from rest_framework_swagger.views import get_swagger_view
from django.conf.urls import url, include
from rest_framework import routers

from account.views import LoginViewSet, LogoutView, RegisterViewSet, UserDeleteViewSet, AccountViewSet, \
                            ListAccountViewSet, InActiveUserViewSet, ChangeRoleUserViewSet, ListUserAccountViewSet, \
                            DepartmentViewSet
from camera.views import TypeViewSet, CameraViewSet
from car.views import CarViewSet
from operation.views import OperationViewSet, CheckCarActivevViewSet
from position.views import PositionViewSet, VideoViewSet
schema_view = get_swagger_view(title='stream_record')
router = routers.DefaultRouter()

router.register(r'login', LoginViewSet)
router.register(r'user/register', RegisterViewSet)
router.register(r'user/delete', UserDeleteViewSet)
router.register(r'user', AccountViewSet)
router.register(r'user/list', ListAccountViewSet)
router.register(r'user/inactive', InActiveUserViewSet)
router.register(r'type', TypeViewSet)
router.register(r'camera', CameraViewSet)
router.register(r'operation', OperationViewSet)
router.register(r'position', PositionViewSet)
router.register(r'car', CarViewSet)
router.register(r'operation/check_car', CheckCarActivevViewSet)
router.register(r'user/changerole', ChangeRoleUserViewSet)
router.register(r'user/list_user', ListUserAccountViewSet)
router.register(r'department', DepartmentViewSet)
router.register(r'video', VideoViewSet)

urlpatterns = [
    # path('admin/', admin.site.urls),
    url(r'admin/', admin.site.urls),
    url(r'^$', schema_view),
    url(r'^logout/$', LogoutView.as_view()),
    url(r'^', include(router.urls))
    # url("^sync/", include(route.urlpatterns))
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
