FROM python:3.6.5
RUN apt-get update
RUN mkdir /app/
WORKDIR /app/
EXPOSE 8000
CMD ["sleep", "365d"]
