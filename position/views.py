from rest_framework import viewsets, mixins
from rest_framework import status
from rest_framework.response import Response
from .models import Video, Position
from django.db.models import Sum, Count
from account.models import Account
from camera.models import Camera
from car.models import Car
from operation.models import Operation
from account.permissions import IsSuperAdmin, IsUserAdmin, IsUser
from rest_framework.decorators import list_route
from rest_framework.permissions import IsAuthenticated
from django.utils import timezone
from .serializers import VideoSerializer, AddVideoSerializer, SelectVideoSerializer, PositionSerializer, \
                        AddPositionSerializer

# Create your views here.

class PositionViewSet(viewsets.GenericViewSet):
    queryset = Position.objects.all()
    action_serializers = {
        'add_position': AddPositionSerializer
    }

    def get_serializer_class(self):
        if hasattr(self, 'action_serializers'):
            if self.action in self.action_serializers:
                return self.action_serializers[self.action]
            return super().get_serializer_class()

    def get_permissions(self):
        if self.action == 'live_position':
            self.permission_classes = [IsUserAdmin, ]
        return super(PositionViewSet, self).get_permissions()

    @list_route(methods=['get'], url_path='list_live_position')
    def live_position(self, request):
        queryset = self.get_queryset()
        queryset = queryset.filter(operation__active=True)
        if request.user.role == 2:
            queryset = queryset.filter(operation__head__department=request.user.department)
        queryset = queryset.values('operation__id').annotate(count=Count('operation__id'))
        response = []
        if queryset:
            for pos in queryset:
                serializer = PositionSerializer(Position.objects.filter(operation__id=pos['operation__id']).last(), many=False)
                response.append(serializer.data)
        return Response(response, status=status.HTTP_200_OK)

    @list_route(methods=['post'], url_path='add_position')
    def add_position(self, request):
        serializer = self.get_serializer(data=request.data)
        queryset = self.get_queryset()
        if serializer.is_valid():
            # car = Car.objects.filter(ip=serializer.data['car_ip'])
            car = Car.objects.filter(name=serializer.data['car_name']).first()
            operation = Operation.objects.filter(car=car)
            operation = operation.filter(active=True).first()
            position = Position.objects.create(
                longitude = serializer.data['longitude'],
                latitude = serializer.data['latitude'],
                car = car,
                operation = operation
            )
            position.save()
            return Response(status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @list_route(methods=['get'], url_path='delete')
    def delete_all_position(self, request):
        queryset = self.get_queryset()
        if queryset:
            for obj in list(queryset):
                obj.delete()
        return Response(status=status.HTTP_200_OK)


class VideoViewSet(viewsets.GenericViewSet):
    queryset = Video.objects.all()
    action_serializers = {
        'add_video': AddVideoSerializer,
        'select_video': SelectVideoSerializer
    }

    def get_serializer_class(self):
        if hasattr(self, 'action_serializers'):
            if self.action in self.action_serializers:
                return self.action_serializers[self.action]
            return super().get_serializer_class()

    def get_permissions(self):
        if self.action == 'select_video':
            self.permission_classes = [IsUserAdmin, ]
        return super(VideoViewSet, self).get_permissions()

    @list_route(methods=['post'], url_path='select_video')
    def select_video(self, request):
        serializer = self.get_serializer(data=request.data)
        queryset = self.get_queryset()
        if serializer.is_valid():
            queryset = queryset.filter(operation=serializer.data['id_operation'])
            queryset = queryset.filter(camera=serializer.data['id_camera'])
            serializer = VideoSerializer(queryset, many=True)
            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @list_route(methods=['post'], url_path='add_video')
    def add_video(self, request):
        serializer = self.get_serializer(data=request.data)
        queryset = self.get_queryset()
        if serializer.is_valid():
            # camera = Camera.objects.filter(ip=serializer.data['camera_ip'])
            camera = Camera.objects.filter(name=serializer.data['camera_name']).first()
            operation = Operation.objects.filter(camera__in=[camera])
            operation = operation.filter(active=True).first()
            video = Video.objects.create(
                name = serializer.data['name'],
                path = serializer.data['path'],
                camera = camera,
                operation = operation
            )
            video.save()
            return Response(status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @list_route(methods=['get'], url_path='delete')
    def delete_all_video(self, request):
        queryset = self.get_queryset()
        if queryset:
            for obj in list(queryset):
                obj.delete()
        return Response(status=status.HTTP_200_OK)
