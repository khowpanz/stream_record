# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from .models import Video, Position
from django.contrib import admin

# Register your models here.
class PositionAdmin(admin.ModelAdmin):
	list_display = ('id', 'latitude', 'longitude', 'create_at', 'get_car', 'get_operation')

	def get_car(self, position):
		return position.car.name if position.car else "-"
	get_car.short_description = 'car'

	def get_operation(self, position):
		return position.operation.name if position.operation else "-"
	get_operation.short_description = 'operation'


class VideoAdmin(admin.ModelAdmin):
	list_display = ('id', 'name', 'path', 'create_at', 'get_camera', 'get_operation')

	def get_camera(self, video):
		return video.camera.name if video.camera else "-"
	get_camera.short_description = 'camera'

	def get_operation(self, video):
		return video.operation.name if video.operation else "-"
	get_operation.short_description = 'operation'


admin.site.register(Position, PositionAdmin)
admin.site.register(Video, VideoAdmin)
