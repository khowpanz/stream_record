from django.db import models
from car.models import Car
from operation.models import Operation
from camera.models import Camera
# Create your models here.

class Position(models.Model):
    longitude = models.DecimalField(max_digits=40, decimal_places=30, default=0)
    latitude = models.DecimalField(max_digits=40, decimal_places=30, default=0)
    create_at = models.DateTimeField(auto_now_add=True)
    car = models.ForeignKey(Car, on_delete=models.CASCADE)
    operation = models.ForeignKey(Operation, on_delete=models.CASCADE)

    # def __str__(self):
    #     return self.create_at


class Video(models.Model):
    name = models.CharField(max_length=200)
    path = models.CharField(max_length=200)
    create_at = models.DateTimeField(auto_now_add=True)
    camera = models.ForeignKey(Camera, on_delete=models.CASCADE)
    operation = models.ForeignKey(Operation, on_delete=models.CASCADE)

    # def __str__(self):
    #     return self.create_at
