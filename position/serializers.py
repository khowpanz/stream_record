from rest_framework import serializers
from .models import Position, Video

class VideoSerializer(serializers.ModelSerializer):
    camera = serializers.SerializerMethodField()
    operation = serializers.SerializerMethodField()
    class Meta:
        model = Video
        fields = ('id', 'create_at', 'name', 'path', 'camera', 'operation')

    def get_camera(self, VDO):
        return VDO.camera.name if VDO.camera else "-"

    def get_operation(self, VDO):
        return VDO.operation.name if VDO.operation else "-"


class PositionSerializer(serializers.ModelSerializer):
    car = serializers.SerializerMethodField()
    operation = serializers.SerializerMethodField()
    id_operation = serializers.SerializerMethodField()
    class Meta:
        model = Position
        fields = ('id_operation', 'create_at', 'latitude', 'longitude', 'car', 'operation')

    def get_car(self, POS):
        return POS.car.name if POS.car else "-"

    def get_operation(self, POS):
        return POS.operation.name if POS.operation else "-"

    def get_id_operation(self, POS):
        return POS.operation.id if POS.operation else "-"


class AddVideoSerializer(serializers.Serializer):
    name = serializers.CharField(max_length=100)
    path = serializers.CharField(max_length=200)
    camera_name = serializers.CharField(max_length=200)
    camera_ip = serializers.CharField(max_length=200)


class SelectVideoSerializer(serializers.Serializer):
    id_operation = serializers.IntegerField(required=True)
    id_camera = serializers.IntegerField(required=True)


class AddPositionSerializer(serializers.Serializer):
    longitude = serializers.DecimalField(max_digits=40, decimal_places=30, default=0)
    latitude = serializers.DecimalField(max_digits=40, decimal_places=30, default=0)
    car_name = serializers.CharField(max_length=200)
    car_ip = serializers.CharField(max_length=200)
