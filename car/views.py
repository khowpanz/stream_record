from rest_framework import viewsets, mixins
from rest_framework import status
from rest_framework.response import Response
from .models import Car
from account.models import Department
from account.permissions import IsSuperAdmin, IsUserAdmin, IsUser
from rest_framework.decorators import list_route
from rest_framework.permissions import IsAuthenticated
from django.utils import timezone
from .serializers import CarSerializer, AddCarSerializer, DeleteCarSerializer, EditCarSerializer, SelectDepartmentSerializer

# Create your views here.

class CarViewSet(mixins.ListModelMixin, viewsets.GenericViewSet):
    queryset = Car.objects.all()
    action_serializers = {
        'add_car': AddCarSerializer,
        'delete_car': DeleteCarSerializer,
        'edit_car': EditCarSerializer,
        'show_car': SelectDepartmentSerializer
    }

    def get_serializer_class(self):
        if hasattr(self, 'action_serializers'):
            if self.action in self.action_serializers:
                return self.action_serializers[self.action]
            return super().get_serializer_class()

    def get_permissions(self):
        if self.action == 'list':
            self.permission_classes = [IsUser, ]
        else:
            self.permission_classes = [IsUserAdmin, ]
        return super(CarViewSet, self).get_permissions()

    def list(self, request):
        queryset = self.get_queryset()
        queryset = queryset.filter(department=request.user.department)
        serializer = CarSerializer(queryset, many=True)
        return Response(serializer.data)

    @list_route(methods=['post'], url_path='show')
    def show_car(self, request):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            queryset = self.get_queryset()
            if request.user.role == 2:
                queryset = queryset.filter(department=request.user.department)
            elif request.user.role == 1:
                queryset = queryset.filter(department=serializer.data['department_pk'])
            serializer = CarSerializer(queryset, many=True)
            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @list_route(methods=['post'], url_path='add')
    def add_car(self, request):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            ip = serializer.data['ip']
            car = Car.objects.filter(ip=ip).first()
            if request.user.role == 2:
                department = Department.objects.filter(name=request.user.department.name).first()
            elif reeust.user.role == 1:
                department = Department.objects.filter(id=serializer.data['department']).first()
            if car:
                return Response("this ip is exist", status=status.HTTP_400_BAD_REQUEST)
            car = Car.objects.create(
                name = serializer.data['name'],
                ip = ip,
                department = department
            )
            car.save()
            return Response(status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @list_route(methods=['post'], url_path='delete')
    def delete_car(self, request):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            id = serializer.data['id']
            car_target = Car.objects.filter(id=id).first()
            if not car_target:
                return Response({'error': 'car for delete not found'}, status=status.HTTP_404_NOT_FOUND)
            car_target.delete()
            return Response(status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @list_route(methods=['post'], url_path='edit')
    def edit_car(self, request):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            id = serializer.data['id']
            car_target = Car.objects.filter(id=id).first()
            if not car_target:
                return Response({'error': 'car for edit not found'}, status=status.HTTP_404_NOT_FOUND)
            if serializer.data['ip']:
                car_target.ip = serializer.data['ip']
            if serializer.data['name']:
                car_target.name = serializer.data['name']
            car_target.save()
            return Response(status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
