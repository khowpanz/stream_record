from __future__ import unicode_literals

from django.db import models
from account.models import Department
# Create your models here.

class Car(models.Model):
    name = models.CharField(max_length=200, null=False)
    ip = models.CharField(max_length=100, null=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    department = models.ForeignKey(Department, null=True, on_delete=models.CASCADE)

    def __str__(self):
        return self.name
