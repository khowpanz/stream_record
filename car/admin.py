# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from .models import Car
from django.contrib import admin

# Register your models here.
class CarAdmin(admin.ModelAdmin):
	list_display = ('id', 'name', 'ip', 'created_at', 'updated_at', 'get_department')

	def get_department(self, car):
		return car.department.name if car.department else "-"
	get_department.short_description = 'department'

admin.site.register(Car, CarAdmin)
