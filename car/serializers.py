from rest_framework import serializers
from .models import Car

class CarSerializer(serializers.ModelSerializer):
    department = serializers.SerializerMethodField()
    class Meta:
        model = Car
        fields = ('id', 'name', 'ip', 'department')

    def get_department(self, CAR):
        return CAR.department.name if CAR.department else "-"


class AddCarSerializer(serializers.Serializer):
    ip = serializers.CharField(max_length=100)
    name = serializers.CharField(max_length=100)
    department = serializers.IntegerField(required=True)


class DeleteCarSerializer(serializers.Serializer):
    id = serializers.IntegerField(required=True)


class SelectDepartmentSerializer(serializers.Serializer):
    department_pk = serializers.IntegerField(allow_null=True)


class EditCarSerializer(serializers.Serializer):
    id = serializers.IntegerField(required=True)
    ip = serializers.CharField(max_length=100, allow_null=True)
    name = serializers.CharField(max_length=100, allow_null=True)
    department = serializers.IntegerField(required=False)
