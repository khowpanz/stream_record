from django.contrib.auth import authenticate, login, logout
from django.conf import settings
from rest_framework import viewsets, mixins
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.authentication import BasicAuthentication
from rest_framework.exceptions import AuthenticationFailed, PermissionDenied, NotAuthenticated
from rest_framework.decorators import list_route
from .authenticate import CsrfExemptSessionAuthentication
from .models import Account, Department
from .permissions import IsSuperAdmin, IsUserAdmin, IsUser
from .serializers import UserLogInSerializer, RegisterSerializer, UserDeleteSerializer, \
                        ChangePasswordSerializer, ChangeNameSerializer, AccountListSerializer, \
                        UserInActiveSerializer, ChangeRoleSerializer, AddDepartmentSerializer, \
                        DeleteDepartmentSerializer, ListDepartmentSerializer, EditDepartmentSerializer, \
                        SelectDepartmentSerializer

class DepartmentViewSet(mixins.ListModelMixin, viewsets.GenericViewSet):
    queryset = Department.objects.all()
    action_serializers = {
        'add_department': AddDepartmentSerializer,
        'delete_department': DeleteDepartmentSerializer,
        'edit_department': EditDepartmentSerializer
    }

    def get_serializer_class(self):
        if hasattr(self, 'action_serializers'):
            if self.action in self.action_serializers:
                return self.action_serializers[self.action]
            return super().get_serializer_class()

    def get_permissions(self):
        self.permission_classes = [IsUserAdmin, ]
        return super(DepartmentViewSet, self).get_permissions()

    def list(self, request):
        queryset = self.get_queryset()
        serializer = ListDepartmentSerializer(queryset, many=True)
        return Response(serializer.data)

    @list_route(methods=['post'], url_path='add')
    def add_department(self, request):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            name = serializer.data['name']
            department = Department.objects.filter(name=name).first()
            if department:
                return Response("this department is exist", status=status.HTTP_400_BAD_REQUEST)
            department = Department.objects.create(
                name = name
            )
            department.save()
            return Response(status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @list_route(methods=['post'], url_path='delete')
    def delete_department(self, request):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            id = serializer.data['id']
            department_target = Department.objects.filter(id=id).first()
            if not department_target:
                return Response({'error': 'department for delete not found'}, status=status.HTTP_404_NOT_FOUND)
            department_target.delete()
            return Response(status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @list_route(methods=['post'], url_path='edit')
    def edit_department(self, request):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            id = serializer.data['department_pk']
            department_target = Department.objects.filter(id=id).first()
            if not department_target:
                return Response({'error': 'department for edit not found'}, status=status.HTTP_404_NOT_FOUND)
            if serializer.data['name']:
                department_target.name = serializer.data['name']
            if serializer.data['active']:
                department_target.active = not department_target.active
            department_target.save()
            return Response(status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class RegisterViewSet(viewsets.GenericViewSet):

    queryset = Account.objects.all()
    serializer_class = RegisterSerializer

    def get_permissions(self):
        if self.action == 'useradmin_register':
            self.permission_classes = [IsUserAdmin, ]
        return super(RegisterViewSet, self).get_permissions()

    @list_route(methods=['post'], url_path='user_register')
    def user_register(self, request):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            email = serializer.data['email']
            account = Account.objects.filter(email=email).first()
            if account:
                return Response("this email is exist", status=status.HTTP_400_BAD_REQUEST)
            if request.user.role == 2:
                department = Department.objects.filter(name=request.user.department.name).first()
            elif request.user.role == 1:
                department = Department.objects.filter(id=serializer.data['department']).first()
            account = Account.objects.create_user(
                email=email,
                first_name=serializer.data['first_name'],
                last_name=serializer.data['last_name'],
                password=serializer.data['password'],
                role=3,
                department=department
            )
            account.set_password(serializer.data['password'])
            account.save()
            return Response(status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @list_route(methods=['post'], url_path='useradmin_register')
    def useradmin_register(self, request):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            email = serializer.data['email']
            account = Account.objects.filter(email=email).first()
            if account:
                return Response("this email is exist", status=status.HTTP_400_BAD_REQUEST)
            if request.user.role == 2:
                department = Department.objects.filter(name=request.user.department.name).first()
            elif request.user.role == 1:
                department = Department.objects.filter(id=serializer.data['department']).first()
            account = Account.objects.create_user(
                email=email,
                first_name=serializer.data['first_name'],
                last_name=serializer.data['last_name'],
                password=serializer.data['password'],
                role=2,
                department=department
            )
            account.set_password(serializer.data['password'])
            account.save()
            return Response(status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class InActiveUserViewSet(mixins.CreateModelMixin, viewsets.GenericViewSet):

    queryset = Account.objects.all()
    serializer_class = UserInActiveSerializer

    def get_permissions(self):
        self.permission_classes = [IsUserAdmin, ]
        return super(InActiveUserViewSet, self).get_permissions()

    def create(self, request):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            check_user = authenticate(email=request.user.email, password=serializer.data['password'])
            if not check_user:
                return Response({'error': 'password not match'}, status=status.HTTP_404_NOT_FOUND)

            user_target = Account.objects.filter(id=serializer.data['user_pk']).first()
            if not user_target:
                return Response({'error': 'user for inactive not found'}, status=status.HTTP_404_NOT_FOUND)
            user_target.active = not user_target.active
            user_target.save()
            return Response(status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ChangeRoleUserViewSet(mixins.CreateModelMixin, viewsets.GenericViewSet):

    queryset = Account.objects.all()
    serializer_class = ChangeRoleSerializer

    def get_permissions(self):
        self.permission_classes = [IsUserAdmin, ]
        return super(ChangeRoleUserViewSet, self).get_permissions()

    def create(self, request):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            department = serializer.data['department']
            check_user = authenticate(email=request.user.email, password=serializer.data['password'])
            if not check_user:
                return Response({'error': 'password not match'}, status=status.HTTP_404_NOT_FOUND)

            user_target = Account.objects.filter(id=serializer.data['user_pk']).first()
            if not user_target:
                return Response({'error': 'user for change_role not found'}, status=status.HTTP_404_NOT_FOUND)
            if not serializer.data['role']==1:
                user_target.role = serializer.data['role']
                user_target.save()
            if department:
                user_target.department = Department.objects.filter(id=department).first()
                user_target.save()
            return Response(status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class UserDeleteViewSet(mixins.CreateModelMixin, viewsets.GenericViewSet):

    queryset = Account.objects.all()
    serializer_class = UserDeleteSerializer

    def get_permissions(self):
        self.permission_classes = [IsUserAdmin, ]
        return super(UserDeleteViewSet, self).get_permissions()

    def create(self, request):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            check_user = authenticate(email=request.user.email, password=serializer.data['password'])
            if not check_user:
                return Response({'error': 'password not match'}, status=status.HTTP_404_NOT_FOUND)

            user_target = Account.objects.filter(id=serializer.data['user_pk']).first()
            if not user_target:
                return Response({'error': 'user for delete not found'}, status=status.HTTP_404_NOT_FOUND)
            user_target.delete()
            return Response(status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ListAccountViewSet(mixins.ListModelMixin, viewsets.GenericViewSet):
    queryset = Account.objects.all()
    action_serializers = {
        'superadmin_list_user': SelectDepartmentSerializer
    }

    def get_serializer_class(self):
        if hasattr(self, 'action_serializers'):
            if self.action in self.action_serializers:
                return self.action_serializers[self.action]
            return super().get_serializer_class()

    def get_permissions(self):
        self.permission_classes = [IsUserAdmin, ]
        return super(ListAccountViewSet, self).get_permissions()

    def list(self, request):
        queryset = self.get_queryset()
        if request.user.role == 2:
            queryset = queryset.filter(role__range=[2,3])
            queryset = queryset.filter(department=request.user.department)
        elif request.user.role == 1:
            queryset = queryset.filter(role__range=[1,3])
        serializer = AccountListSerializer(queryset, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    @list_route(methods=['post'], url_path='superadmin_list_user')
    def superadmin_list_user(self, request):
        queryset = self.get_queryset()
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            queryset = queryset.filter(role__range=[2,3])
            queryset = queryset.filter(department=serializer.data['department_pk'])
            serializer = AccountListSerializer(queryset, many=True)
            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ListUserAccountViewSet(mixins.ListModelMixin, viewsets.GenericViewSet):
    queryset = Account.objects.all()
    def get_permissions(self):
        self.permission_classes = [IsUser, ]
        return super(ListUserAccountViewSet, self).get_permissions()

    def list(self, request):
        queryset = self.get_queryset()
        queryset = queryset.filter(role=3)
        serializer = AccountListSerializer(queryset, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class LoginViewSet(mixins.CreateModelMixin, viewsets.GenericViewSet):

    queryset = Account.objects.all()
    allow_redirects = True
    authentication_classes = (
        CsrfExemptSessionAuthentication, BasicAuthentication,)
    serializer_class = UserLogInSerializer

    def create(self, request, *args, **kwargs):
        import re
        if request.user.is_authenticated:
            raise PermissionDenied('Please logout.')

        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            email = serializer.data.get('email', None)
            password = serializer.data.get('password')

            email_regex = re.compile(r'[^@]+@[^@]+\.[^@]+')
            if email_regex.match(email):
                msg = 'This email or password is not valid.'
            else:
                msg = 'This email is wrong format'

            user = authenticate(email=email, password=password)
            if not user:
                return Response({"detail": msg}, status=status.HTTP_400_BAD_REQUEST)
            if not user.active:
                return Response({"detail": "This user not active"}, status=status.HTTP_400_BAD_REQUEST)
            if user.role >=2:
                if not user.department.active:
                    return Response({"detail": "This department of user not active"}, status=status.HTTP_400_BAD_REQUEST)


            login(request, user)
            request.session['ip'] = request.META.get('REMOTE_ADDR')
            request.session.set_expiry(3153600000)

            response = { "role": user.role }
            return Response(response, status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class AccountViewSet(viewsets.GenericViewSet):

    queryset = Account.objects.all()

    action_serializers = {
        'change_password': ChangePasswordSerializer,
        'change_name': ChangeNameSerializer
    }

    def get_serializer_class(self):
        if hasattr(self, 'action_serializers'):
            if self.action in self.action_serializers:
                return self.action_serializers[self.action]
            return super().get_serializer_class()

    def get_permissions(self):
        self.permission_classes = [IsAuthenticated, ]
        return super(AccountViewSet, self).get_permissions()

    @list_route(methods=['post'], url_path='change-password')
    def change_password(self, request):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            check_user = authenticate(email=request.user.email, password=serializer.data['old_password'])
            if not check_user:
                return Response({'error': 'password not match'}, status=status.HTTP_404_NOT_FOUND)
            check_user.set_password(serializer.data['new_password'])
            check_user.save()
            return Response(status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @list_route(methods=['post'], url_path='change-name')
    def change_name(self, request):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            user = Account.objects.filter(id=request.user.id).first()
            if serializer.data['first_name']:
                user.first_name = serializer.data['first_name']
            if  serializer.data['last_name']:
                user.last_name = serializer.data['last_name']
            user.save()
            return Response(status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class LogoutView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        if request.user.is_authenticated:
            logout(request)
            return Response(status=status.HTTP_200_OK)
        else:
            raise NotAuthenticated('Please login.')
