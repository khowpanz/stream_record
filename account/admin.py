# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from .models import Account, Department

# Register your models here.
class AccountAdmin(admin.ModelAdmin):
    list_display = ('id', 'email', 'first_name', 'last_name', 'get_role_display', 'department')


class DepartmentAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'active')

admin.site.register(Account, AccountAdmin)
admin.site.register(Department, DepartmentAdmin)
