from rest_framework import serializers
from rest_framework.serializers import raise_errors_on_nested_writes, model_meta

from account.models import Account, Department

class AddDepartmentSerializer(serializers.Serializer):
    name = serializers.CharField(max_length=255)


class DeleteDepartmentSerializer(serializers.Serializer):
    department_pk = serializers.IntegerField(required=True)


class SelectDepartmentSerializer(serializers.Serializer):
    department_pk = serializers.IntegerField(required=True)


class EditDepartmentSerializer(serializers.Serializer):
    department_pk = serializers.IntegerField(required=True)
    name = serializers.CharField(max_length=255,  allow_blank=True)
    active = serializers.BooleanField(required=True)


class ListDepartmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Department
        fields = ('id', 'name', 'active')


class RegisterSerializer(serializers.Serializer):
    email = serializers.EmailField(max_length=255, allow_blank=False)
    first_name = serializers.CharField(max_length=255)
    last_name = serializers.CharField(max_length=255)
    password = serializers.CharField(min_length=4)
    department = serializers.IntegerField(allow_null=True)


class UserLogInSerializer(serializers.Serializer):
    email = serializers.CharField(max_length=255)
    password = serializers.CharField(min_length=4)


class UserDeleteSerializer(serializers.Serializer):
    user_pk = serializers.IntegerField(required=True)
    password = serializers.CharField(required=True)


class ChangeRoleSerializer(serializers.Serializer):
    user_pk = serializers.IntegerField(required=True)
    password = serializers.CharField(required=True)
    role = serializers.IntegerField(allow_null=True)
    department = serializers.IntegerField(allow_null=True)


class UserInActiveSerializer(serializers.Serializer):
    user_pk = serializers.IntegerField(required=True)
    password = serializers.CharField(required=True)


class ChangePasswordSerializer(serializers.Serializer):
    old_password = serializers.CharField(min_length=4, required=True)
    new_password = serializers.CharField(min_length=4, required=True)


class ChangeNameSerializer(serializers.Serializer):
    first_name = serializers.CharField(max_length=255, required=True, allow_blank=True)
    last_name = serializers.CharField(max_length=255, required=True, allow_blank=True)


class AccountListSerializer(serializers.ModelSerializer):
    role = serializers.SerializerMethodField()
    class Meta:
        model = Account
        fields = ('id', 'email', 'first_name', 'last_name', 'role', 'active')

    def get_role(self, AC):
        return AC.get_role_display()
