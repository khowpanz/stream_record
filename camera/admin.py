# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from .models import Camera, Type
from account.models import Department
from django.contrib import admin

# Register your models here.
class TypeAdmin(admin.ModelAdmin):
	list_display = ('id', 'name')

class CameraAdmin(admin.ModelAdmin):
	list_display = ('id', 'name', 'ip', 'created_at', 'updated_at', 'get_type', 'get_department')

	def get_type(self, camera):
		return camera.type.name if camera.type else "-"
	get_type.short_description = 'type'

	def get_department(self, camera):
		return camera.department.name if camera.department else "-"
	get_type.short_description = 'department'

admin.site.register(Camera, CameraAdmin)
admin.site.register(Type, TypeAdmin)
