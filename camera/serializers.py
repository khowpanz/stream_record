from rest_framework import serializers
from .models import Camera, Type

class TypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Type
        fields = ('id', 'name')


class CameraSerializer(serializers.ModelSerializer):
    type_name = serializers.SerializerMethodField()
    department = serializers.SerializerMethodField()
    class Meta:
        model = Camera
        fields = ('id', 'type_name', 'name', 'ip', 'department')

    def get_type_name(self, CD):
        return CD.type.name if CD.type else "-"

    def get_department(self, CD):
        return CD.department.name if CD.department else "-"


class AddCameraSerializer(serializers.Serializer):
    ip = serializers.CharField(max_length=100)
    name = serializers.CharField(max_length=100)
    type = serializers.IntegerField(required=True)
    department = serializers.IntegerField(required=True)


class DeleteCameraSerializer(serializers.Serializer):
    id = serializers.IntegerField(required=True)


class SelectDepartmentSerializer(serializers.Serializer):
    department_pk = serializers.IntegerField(allow_null=True)


class EditCameraSerializer(serializers.Serializer):
    id = serializers.IntegerField(required=True)
    ip = serializers.CharField(max_length=100, allow_null=True)
    name = serializers.CharField(max_length=100, allow_null=True)
    type = serializers.IntegerField(allow_null=True)
    department = serializers.IntegerField(allow_null=True)


class AddTypeSerializer(serializers.Serializer):
    name = serializers.CharField(max_length=200)


class DeleteTypeSerializer(serializers.Serializer):
    id = serializers.IntegerField(required=True)
