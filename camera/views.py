from rest_framework import viewsets, mixins
from rest_framework import status
from rest_framework.response import Response
from django.db.models import Sum, Count
from .models import Type, Camera
from account.models import Account, Department
from account.permissions import IsSuperAdmin, IsUserAdmin, IsUser
from rest_framework.decorators import list_route
from rest_framework.permissions import IsAuthenticated
from django.utils import timezone
from .serializers import TypeSerializer, CameraSerializer, AddCameraSerializer, DeleteCameraSerializer, \
                        EditCameraSerializer, AddTypeSerializer, DeleteTypeSerializer, SelectDepartmentSerializer

from decimal import Decimal

# Create your views here.

class TypeViewSet(mixins.ListModelMixin, viewsets.GenericViewSet):
    queryset = Type.objects.all()
    action_serializers = {
        'list_type': TypeSerializer,
        'add_type': AddTypeSerializer,
        'delete_type': DeleteTypeSerializer
    }

    def get_serializer_class(self):
        if hasattr(self, 'action_serializers'):
            if self.action in self.action_serializers:
                return self.action_serializers[self.action]
            return super().get_serializer_class()

    def get_permissions(self):
        self.permission_classes = [IsUserAdmin, ]
        return super(TypeViewSet, self).get_permissions()

    def list(self, request):
        queryset = self.get_queryset()
        serializer = TypeSerializer(queryset, many=True)
        return Response(serializer.data)

    @list_route(methods=['post'], url_path='add')
    def add_type(self, request):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            name = serializer.data['name']
            type = Type.objects.filter(name=name).first()
            if type:
                return Response("this type is exist", status=status.HTTP_400_BAD_REQUEST)
            type = Type.objects.create(
                name = name
            )
            type.save()
            return Response(status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @list_route(methods=['post'], url_path='delete')
    def delete_type(self, request):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            id = serializer.data['id']
            type_target = Type.objects.filter(id=id).first()
            if not type_target:
                return Response({'error': 'type for delete not found'}, status=status.HTTP_404_NOT_FOUND)
            type_target.delete()
            return Response(status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class CameraViewSet(mixins.ListModelMixin, viewsets.GenericViewSet):
    queryset = Camera.objects.all()
    action_serializers = {
        'add_camera': AddCameraSerializer,
        'delete_camera': DeleteCameraSerializer,
        'edit_camera': EditCameraSerializer,
        'show_camera': SelectDepartmentSerializer,
        'camera_with_type': SelectDepartmentSerializer
    }

    def get_serializer_class(self):
        if hasattr(self, 'action_serializers'):
            if self.action in self.action_serializers:
                return self.action_serializers[self.action]
            return super().get_serializer_class()

    def get_permissions(self):
        if self.action == 'camera_with_type':
            self.permission_classes = [IsUser, ]
        else:
            self.permission_classes = [IsUserAdmin, ]
        return super(CameraViewSet, self).get_permissions()

    def list(self, request):
        queryset = self.get_queryset()
        serializer = CameraSerializer(queryset, many=True)
        return Response(serializer.data)

    @list_route(methods=['post'], url_path='show')
    def show_camera(self, request):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            queryset = self.get_queryset()
            if request.user.role == 2:
                queryset = queryset.filter(department=request.user.department)
            elif request.user.role == 1:
                queryset = queryset.filter(department=serializer.data['department_pk'])
            serializer = CameraSerializer(queryset, many=True)
            return Response(serializer.data, status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @list_route(methods=['post'], url_path='camera_with_type')
    def camera_with_type(self, request):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            queryset = self.get_queryset()
            if request.user.role == 2:
                queryset = queryset.filter(department=request.user.department)
            elif request.user.role == 1:
                queryset = queryset.filter(department=serializer.data['department_pk'])
            type_all = queryset.values('type').annotate(count=Count('type'))
            response = []
            type_camera = []
            if type_all:
                for type_name in type_all:
                    type = Type.objects.filter(id=type_name['type']).first()
                    type_camera.append(type.name)
                    response.append({ type.name : []})
            for res, type_cam in zip(response, type_camera):
                camera = queryset.filter(type__name=str(type_cam))
                serializer = CameraSerializer(camera, many=True)
                res.update({ type_cam : serializer.data})
            return Response(response)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @list_route(methods=['post'], url_path='add')
    def add_camera(self, request):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            ip = serializer.data['ip']
            type = Type.objects.filter(id=serializer.data['type']).first()
            if request.user.role == 2:
                department = Department.objects.filter(name=request.user.department.name).first()
            elif reeust.user.role == 1:
                department = Department.objects.filter(id=serializer.data['department']).first()
            camera = Camera.objects.filter(ip=ip).first()
            if camera:
                return Response("this ip is exist", status=status.HTTP_400_BAD_REQUEST)
            camera = Camera.objects.create(
                name = serializer.data['name'],
                ip = ip,
                type = type,
                department = department
            )
            camera.save()
            return Response(status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @list_route(methods=['post'], url_path='delete')
    def delete_camera(self, request):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            id = serializer.data['id']
            camera_target = Camera.objects.filter(id=id).first()
            if not camera_target:
                return Response({'error': 'camera for delete not found'}, status=status.HTTP_404_NOT_FOUND)
            camera_target.delete()
            return Response(status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @list_route(methods=['post'], url_path='edit')
    def edit_camera(self, request):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            id = serializer.data['id']
            camera_target = Camera.objects.filter(id=id).first()
            if not camera_target:
                return Response({'error': 'camera for edit not found'}, status=status.HTTP_404_NOT_FOUND)
            if serializer.data['ip']:
                camera_target.ip = serializer.data['ip']
            if serializer.data['name']:
                camera_target.name = serializer.data['name']
            if serializer.data['type']:
                camera_target.type = Type.objects.filter(id=serializer.data['type']).first()
            if serializer.data['department']:
                camera_target.type = Department.objects.filter(id=serializer.data['department']).first()
            camera_target.save()
            return Response(status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
