# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from account.models import Department
# Create your models here.

class Type(models.Model):
    name = models.CharField(max_length=200, null=False)

    def __str__(self):
        return self.name


class Camera(models.Model):
    ip = models.CharField(max_length=100, null=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    name = models.CharField(max_length=200, null=False)
    type = models.ForeignKey(Type, null=True, on_delete=models.CASCADE)
    department = models.ForeignKey(Department, null=True, on_delete=models.CASCADE)

    def __str__(self):
        return self.name
